---
pagetitle: "Lecture 2"
title: "\nTaller de excel\n"
subtitle: "\nLectura 2: Introducción a la interfaz de Excel\n"
author: "\nEduard F Martínez-González\n"
date: "Universidad de los Andes | [ECON-1300](https://eduard-martinez.github.io/teaching/excel/2022-01/website.html)"
output: 
  revealjs::revealjs_presentation:  
    theme: simple 
    highlight: tango
    center: true
    nature:
      transition: slide
      self_contained: false # para que funcione sin internet
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: true
      showSlideNumber: 'all'
    seal: true # remover la primera diapositiva (https://github.com/yihui/xaringan/issues/84)
    # Help 1: https://revealjs.com/presentation-size/  
    # Help 2: https://bookdown.org/yihui/rmarkdown/revealjs.html
    # Estilos revealjs: https://github.com/hakimel/reveal.js/blob/master/css/theme/template/theme.scss
---

## Hoy veremos

### **1.** Libros y Hojas. 

### **2.** Filas y Columnas.

### **3.** Celdas, Rangos y Tablas.

### **4.** Atajos de teclado [[Ver aquí]](https://lectures-excel.gitlab.io/202201/shortcut/#/)

<!--------------------->
<!---Libros y Hojas.--->
<!--------------------->

# [1.] Libros y Hojas
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!--------------------->
## 1.1 Libros
los documentos de Excel también reciben el nombre de libros, los cuales cuentan con hojas de almacenamiento y procesamiento de datos. dentro de los cuales podemos encontrar;

    * XLSX
    * XLSM
    * XLTX
    * XLTM
    * XLAM
    * XLSB

<!--------------------->
## 1.2 Hojas
Algunos de los comandos más básicos para un buen manejo de la herramienta Excel son;

    * Agregar hoja nueva; Shift + f1
    * Editar nombre de hoja; clic derecho -> cambiar nombre
    * Editar dorsal de hoja; clic derecho -> color de pestaña
    * Seleccionar un grupo de hojas; Ctrl + selección 

<!--------------------------->
<!------Filas y Columnas.---->
<!--------------------------->

# [2.] Filas y Columnas
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!--------------------->
## 2.1 ¿Que son las filas y columnas?

* Filas; las encontramos dispuestas de manera horizontal y las podemos identificar por sus numeraciones del 1 - 1´048,576.
* columnas; están dispuestas de manera vertical y se identifican con las letras de la A-Z con combinaciones entre ellas para columnas posteriores a la Z.
  
![](pics/imagen.jpg)    

<!--------------------->
## 2.2 Menú "Celdas"

Se encuentra en la pestaña "inicio" de la cinta de opciones de Excel, donde con una serie de acciones 

![](pics/imagen 2.jpg)

<!--------------------->
## 2.3 Manejo de filas y columnas de Excel 

#### 2.3.1 Seleccionar 
 * Clic; seleccionar una fila o columna 
 * Ctrl (CMD) + clic; seleccionar F o C individual
 * Shift + clic; seleccionar rango de F o C
 * Shift + barra de espacio; seleccionar fila 
 * Ctrl (CMD) + barra de espacio; seleccionar columna
 
#### 2.3.2 Mover
 * Arrastrar + Shift; intercambia filas y columnas 
 * Arrastrar + Ctrl; duplica o reemplaza F o C
 * Arrastrar + Shift + Ctrl; copia y añade F o C 

<!--------------------->
## 2.3 Manejo de filas y columnas de Excel (cont.)

#### 2.3.3 Insertar y eliminar 
* **2.3.3.1 Insertar:**
 * Clic derecho -> insertar 
 * Ctrl + "+"
* **2.3.3.2 Eliminar:**
 * clic derecho -> eliminar
 * Ctrl + "-"

#### 2.3.4 Ocultar y mostrar 
* **Ocultar:**
 * Ctrl + 0; oculta filas
 * Ctrl + 9; oculta columnas
* **Mostrar:**
 * Ctrl + Shift + 8; muestra filas
 * Ctrl + Shift + 9; muestra columnas
 
<!--------------------->
## 2.4 Movilización de paneles en Excel 
![](pics/imagen 3.jpg)

<!--------------------->
## 2.5 Edición de alto y hancho
##### **Autoajuste:**
![](pics/imagen 4.jpg)

##### **Manual:**
![](pics/imagen 5.jpg)
Las filas tienen inicialmente un alto predeterminado de 15 y puede llegr hasta 409. Y las columnas tienen inicialmente un ancho predeterminado de 10.71 y puede llegr hasta 255.


<!--------------------------->
<!--Celdas, Rangos y Tablas-->
<!--------------------------->

# [3.] Celdas, Rangos y Tablas
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!--------------------->
## 3.1 ¿qué es una celda en Excel?
Una celda es la intersección entre una fila y una columna. es el espacio que contiene algún tipo de dato (caracteres, números, fechas, funciones, etc...) y se ubica fácilmente con el numero de fila y letra de columna. 

![](pics/imagen 6.jpg)
 
<!---------------------> 
## 3.1.1 ¿Cómo editar o ajustar datos dentro de las celdas?

![](pics/imagen 7.jpg)

<!---------------------> 
## 3.2 ¿Qué es un rango en Excel?
Es un grupo de celdas fronterizas delimitadas por dos celdas las cuales se pueden describir por la letra donde se encuentran, seguido de su numero. por ejemplo A1;C2 
![](pics/imagen 8.jpg)

#### 3.2.1 Tipos de rangos 
 * Unidimensional; cuando se referencia de una sola celda
 * Bidimensional; cuando se referencian dos celdas 
 * Tridimensional; cuando se referencia una celda o un grupo de celdas que comienzan en una hoja y terminan en otra.
 
#### 3.2.2 Selección y manejo de rangos 
 * podemos realizar un clic izquierdo y arrastrar el raton en la direccion deseada para abarcar todas las celdas
 * presionar tecla Shift y moverse con las flechas hacia la direccion deseada
 

<!---------------------> 

## 3.3 Tablas en Excel 
 * las tablas son un conjunto de filas y columnas que contienen cierto tipo de datos relacionados y que son manejados de manera indemendiente
 * las tablas son la manera en que excel identifica un rango de celdas y examina que la información contenida en ella este relacionada
 
 
<!--------------------->
<!--- Checklist --->
<!--------------------->

# [4.] Task
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

* En un libro nuevo de excel, cree 3 hojas nuevas, renombrelas (como usted prefiera) y cambie el color de las pestañas de la hoja.
* Cambie el color de las celdas B7:H7
* Aplique un formato númerico a las celdas del rango B8:H20
* Inmobilice las filas y columnas por encima de la celda C7
* Oculte las filas 2,3,4 y la columna B 
* Renombre el rango B7:H7 como "Nuevo rango"
* Guarde el libro en formato .xlsx y asignele su código como nombre del archivo. Por ejemplo: **201725842.xlsx**

<!--------------------->
<!--- Checklist --->
<!--------------------->
# Gracias
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!--------------------->
## Hoy vimos...

☑  Libros y Hojas. 

☑  Filas y Columnas.

☑  Celdas, Rangos y Tablas.

☑  Atajos de teclado


<!--- HTML style --->
<style type="text/css">
.reveal .progress {background: #CC0000 ; color: #CC0000}
.reveal .controls {color: #CC0000}
.reveal h1.title {font-size: 2.4em;color: #CC0000; font-weight: bolde}
.reveal h1.subtitle {font-size:2.0em ; color:#000000}
.reveal section h1 {font-size:2.0em ; color:#CC0000 ; font-weight:bolder ; vertical-align:middle}
.reveal section h2 {font-size:1.3em ; color:#CC0000 ; font-weight:bolder ; text-align:left}
.reveal section h3 {font-size:0.9em ; color:#00000 ; font-weight:bolder ; text-align:left}
.reveal section h4 {font-size:0.9em ; color:#CC0000 ; text-align:left}
.reveal section h5 {font-size:0.9em ; color:#00000 ; font-weight:bolder ; text-align:left}
.reveal section p {font-size:0.7em ; color:#00000 ; text-align:left}
.reveal section a {font-size:0.9em ; color:#000099 ; text-align:left}
.reveal section href {font-size:0.9em ; color:#000099 ; text-align:left}
.reveal section div {align="center";}
.reveal ul {list-style-type:disc ; font-size:0.8em ; color:#00000 ; display: block;}
.reveal ul ul {list-style-type: square; font-size:0.8em ; display: block;}
.reveal ul ul ul {list-style-type: circle; font-size:0.8em ; display: block;}
.reveal section img {display: inline-block; border: 0px; background-color: #FFFFFF; align="center"}
</style>


